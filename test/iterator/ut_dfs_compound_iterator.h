#include "../../src/compound_shape.h"
#include "../../src/shape.h"
#include "../../src/rectangle.h"
#include "../../src/iterator/factory/dfs_iterator_factory.h"

class DFSCompoundIteratorTest : public ::testing::Test
{
protected:
    Point *p1, *p2, *p3, *p4;
    TwoDimensionalVector *vec1, *vec2, *vec3;
    Circle *cir1, *cir2;
    Rectangle* rec;
    CompoundShape *cs1, *cs2;
    DFSIteratorFactory* dfs_factory;
    Iterator* it;

    void SetUp() override
    {
        p1 = new Point(0, 0);
        p2 = new Point(0, 5);
        p3 = new Point(5, 0);
        p4 = new Point(0, 3);

        vec1 = new TwoDimensionalVector(p1, p2);
        vec2 = new TwoDimensionalVector(p1, p3);
        vec3 = new TwoDimensionalVector(p1, p4);

        cs1 = new CompoundShape();
        cir1 = new Circle(vec1);
        rec = new Rectangle(vec1,vec2);
        cs1->addShape(cir1);
        cs1->addShape(rec);

        cs2 = new CompoundShape();
        cir2 = new Circle(vec3);
        cs2->addShape(cir2);
        cs2->addShape(cs1);

        dfs_factory = new DFSIteratorFactory();
        it = cs2->createIterator(dfs_factory);
    }

    void TearDown() override
    {
        delete p1;
        delete p2;
        delete p3;
        delete p4;
        delete vec1;
        delete vec2;
        delete vec3;
        delete cs1;
        delete cs2;
        delete cir1;
        delete cir2;
        delete rec;
        delete dfs_factory;
        delete it;
    }
};

TEST_F(DFSCompoundIteratorTest, CurrentItemShouldBeCorrect)
{
    ASSERT_EQ(3 * 3 * M_PI, it->currentItem()->area());
}

TEST_F(DFSCompoundIteratorTest, NextShouldBeCorrect)
{
    it->next();
    ASSERT_EQ(5 * 5 * M_PI + 25, it->currentItem()->area());
    it->next();
    ASSERT_EQ(5 * 5 * M_PI, it->currentItem()->area());
}


TEST_F(DFSCompoundIteratorTest, IsDoneShouldBeCorrect)
{
    it->next();
    it->next();
    it->next();
    it->next();

    ASSERT_TRUE(it->isDone());
}

TEST_F(DFSCompoundIteratorTest, CurrentItemShouldThrowExceptionWhenIsDone)
{
    it->next();
    it->next();
    it->next();
    it->next();

    ASSERT_ANY_THROW(it->next());
}

TEST_F(DFSCompoundIteratorTest, NextShouldThrowExceptionWhenIsDone)
{
    it->next();
    it->next();
    it->next();
    it->next();
    
    ASSERT_ANY_THROW(it->currentItem());
}

TEST_F(DFSCompoundIteratorTest, OrderShouldBeCorrectIfNoChildrenInCompound){
    CompoundShape* compound_shape = new CompoundShape();
    DFSIteratorFactory* dfs_factory = new DFSIteratorFactory();
    Iterator* it = compound_shape->createIterator(dfs_factory);
    ASSERT_TRUE(it->isDone());
    delete compound_shape;
    delete dfs_factory;
    delete it;
}

TEST_F(DFSCompoundIteratorTest, ManyCompoundTest){
    Circle* c1 = new Circle(vec1);
    Circle* c2 = new Circle(vec2);
    Circle* c3 = new Circle(vec3);
    Rectangle* r1 = new Rectangle(vec1, vec2);
    CompoundShape* compound_shape_one = new CompoundShape();
    compound_shape_one->addShape(c1);
    compound_shape_one->addShape(r1);
    CompoundShape* compound_shape_two = new CompoundShape();
    compound_shape_two->addShape(c2);
    compound_shape_two->addShape(c3);
    CompoundShape* compound_shape_three = new CompoundShape();
    compound_shape_three->addShape(compound_shape_one);
    compound_shape_three->addShape(compound_shape_two);
    CompoundShape* compound_shape_four = new CompoundShape();
    compound_shape_four->addShape(compound_shape_two);
    compound_shape_four->addShape(compound_shape_one);
    CompoundShape* compound_shape_five = new CompoundShape();
    compound_shape_five->addShape(compound_shape_three);
    compound_shape_five->addShape(compound_shape_four);
    
    DFSIteratorFactory* dfs_factory = new DFSIteratorFactory();
    Iterator* it = compound_shape_five->createIterator(dfs_factory);
    it->next();
    it->next();
    ASSERT_EQ(5 * 5 * M_PI, it->currentItem()->area());
    it->next();
    ASSERT_EQ(25, it->currentItem()->area());
    it->next();
    ASSERT_EQ((3 * 3 + 5 * 5) * M_PI, it->currentItem()->area());

    delete c1;
    delete c2;
    delete c3;
    delete r1;
    delete compound_shape_one;
    delete compound_shape_two;
    delete compound_shape_three;
    delete compound_shape_four;
    delete compound_shape_five;
    delete dfs_factory;
    delete it;

}
