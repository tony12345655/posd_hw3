#include "../../src/compound_shape.h"
#include "../../src/shape.h"
#include "../../src/rectangle.h"
#include "../../src/iterator/factory/bfs_iterator_factory.h"

class BFSCompoundIteratorTest : public ::testing::Test
{
protected:
    Point *p1, *p2, *p3, *p4;
    TwoDimensionalVector *vec1, *vec2, *vec3;
    Circle* cir1;
    Circle* cir2;
    Rectangle* rec;
    CompoundShape *cs1, *cs2;
    BFSIteratorFactory* bfs_factory;
    Iterator* it;

    void SetUp() override
    {
        p1 = new Point(0, 0);
        p2 = new Point(0, 5);
        p3 = new Point(5, 0);
        p4 = new Point(0, 3);

        vec1 = new TwoDimensionalVector(p1, p2);
        vec2 = new TwoDimensionalVector(p1, p3);
        vec3 = new TwoDimensionalVector(p1, p4);

        cs1 = new CompoundShape();
        cir1 = new Circle(vec1);
        rec = new Rectangle(vec1,vec2);
        cs1->addShape(cir1);
        cs1->addShape(rec);

        cs2 = new CompoundShape();
        cir2 = new Circle(vec3);
        cs2->addShape(cir2);
        cs2->addShape(cs1);

        bfs_factory = new BFSIteratorFactory();
        it = cs2->createIterator(bfs_factory);
    }

    void TearDown() override
    {
        delete p1;
        delete p2;
        delete p3;
        delete p4;
        delete vec1;
        delete vec2;
        delete vec3;
        delete cir1;
        delete cir2;
        delete rec;
        delete cs1;
        delete cs2;
        delete bfs_factory;
        delete it;
    }
};

TEST_F(BFSCompoundIteratorTest, CurrentItemShouldBeCorrect)
{
    ASSERT_EQ(3 * 3 * M_PI, it->currentItem()->area());
}

TEST_F(BFSCompoundIteratorTest, NextShouldBeCorrect)
{
    it->next();
    ASSERT_EQ(5 * 5 * M_PI + 25, it->currentItem()->area());
    it->next();
    ASSERT_EQ(5 * 5 * M_PI, it->currentItem()->area());
}

TEST_F(BFSCompoundIteratorTest, IsDoneShouldBeCorrect)
{
    it->next();
    it->next();
    it->next();
    it->next();

    ASSERT_TRUE(it->isDone());
}

TEST_F(BFSCompoundIteratorTest, CurrentItemShouldThrowExceptionWhenIsDone)
{
    it->next();
    it->next();
    it->next();
    it->next();

    ASSERT_ANY_THROW(it->next());
}

TEST_F(BFSCompoundIteratorTest, NextShouldThrowExceptionWhenIsDone)
{
    it->next();
    it->next();
    it->next();
    it->next();
    
    ASSERT_ANY_THROW(it->currentItem());
}

TEST_F(BFSCompoundIteratorTest, OrderShouldBeCorrectIfNoChildrenInCompound){
    CompoundShape* compound_shape = new CompoundShape();
    BFSIteratorFactory* bfs_factory = new BFSIteratorFactory();
    Iterator* it = compound_shape->createIterator(bfs_factory);
    ASSERT_TRUE(it->isDone());
    delete compound_shape;
    delete bfs_factory;
    delete it;
}

TEST_F(BFSCompoundIteratorTest, ManyCompoundTest){
    Circle* c1 = new Circle(vec1);
    Circle* c2 = new Circle(vec2);
    Circle* c3 = new Circle(vec3);
    Rectangle* r1 = new Rectangle(vec1, vec2);
    CompoundShape* compound_shape_one = new CompoundShape();
    compound_shape_one->addShape(c1);
    compound_shape_one->addShape(r1);
    CompoundShape* compound_shape_two = new CompoundShape();
    compound_shape_two->addShape(c2);
    compound_shape_two->addShape(c3);
    CompoundShape* compound_shape_three = new CompoundShape();
    compound_shape_three->addShape(compound_shape_one);
    compound_shape_three->addShape(compound_shape_two);
    CompoundShape* compound_shape_four = new CompoundShape();
    compound_shape_four->addShape(compound_shape_two);
    compound_shape_four->addShape(compound_shape_one);
    CompoundShape* compound_shape_five = new CompoundShape();
    compound_shape_five->addShape(compound_shape_three);
    compound_shape_five->addShape(compound_shape_four);
    
    BFSIteratorFactory* bfs_factory = new BFSIteratorFactory();
    Iterator* it = compound_shape_five->createIterator(bfs_factory);
    ASSERT_EQ((5 * 5 + 5 * 5 + 3 * 3) * M_PI + 25, it->currentItem()->area());
    it->next();
    ASSERT_EQ((5 * 5 + 5 * 5 + 3 * 3) * M_PI + 25, it->currentItem()->area());
    it->next();
    ASSERT_EQ(5 * 5 * M_PI + 25, it->currentItem()->area());
    it->next();
    ASSERT_EQ((5 * 5 + 3 * 3) * M_PI, it->currentItem()->area());
    it->next();
    ASSERT_EQ((5 * 5 + 3 * 3) * M_PI, it->currentItem()->area());
    it->next();
    ASSERT_EQ(5 * 5 * M_PI + 25, it->currentItem()->area());
    it->next();
    ASSERT_EQ(5 * 5 * M_PI, it->currentItem()->area());
    it->next();
    ASSERT_EQ(25, it->currentItem()->area());
    it->next();
    ASSERT_EQ(5 * 5 * M_PI, it->currentItem()->area());
    it->next();
    ASSERT_EQ(3 * 3 * M_PI, it->currentItem()->area());
    it->next();
    ASSERT_EQ(5 * 5 * M_PI, it->currentItem()->area());
    it->next();
    ASSERT_EQ(3 * 3 * M_PI, it->currentItem()->area());
    it->next();
    ASSERT_EQ(5 * 5 * M_PI, it->currentItem()->area());
    it->next();
    ASSERT_EQ(25, it->currentItem()->area());
    it->next();
    ASSERT_TRUE(it->isDone());
    
    delete c1;
    delete c2;
    delete c3;
    delete r1;
    delete compound_shape_one;
    delete compound_shape_two;
    delete compound_shape_three;
    delete compound_shape_four;
    delete compound_shape_five;
    delete bfs_factory;
    delete it;

}
