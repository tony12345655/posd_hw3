#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/circle.h"
#include "../src/iterator/factory/dfs_iterator_factory.h"
#include "../src/iterator/factory/bfs_iterator_factory.h"
#include "../src/iterator/factory/list_iterator_factory.h"
#include "../src/visitor/collision_detector.h"

#include <set>
#include <algorithm>

TEST(CircleTest, RadiusTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    TwoDimensionalVector* vec = new TwoDimensionalVector(p1, p2);
    Circle c(vec);
    ASSERT_NEAR(1, c.radius(), 0.001);
    delete p1;
    delete p2;
    delete vec;
    
}

TEST(CircleTest, AreaTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    TwoDimensionalVector* vec = new TwoDimensionalVector(p1, p2);
    Circle c(vec);
    ASSERT_NEAR(3.1415, c.area(), 0.001);
    delete p1;
    delete p2;
    delete vec;
}

TEST(CircleTest, PerimeterTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    TwoDimensionalVector* vec = new TwoDimensionalVector(p1, p2);
    Circle c(vec);
    ASSERT_NEAR(6.2831, c.perimeter(), 0.001);
    delete p1;
    delete p2;
    delete vec;
}

TEST(CircleTest, InfoTest){
    Point* p1 = new Point(-4.28, 0.26);
    Point* p2 = new Point(-4.83, 0.73);
    TwoDimensionalVector* vec = new TwoDimensionalVector(p1, p2);
    Circle c(vec);
    ASSERT_EQ("Circle (Vector ((-4.28, 0.26), (-4.83, 0.73)))", c.info());
    delete p1;
    delete p2;
    delete vec;
}

TEST(CircleTest, CreateDFSIteratorTest){
    Point* p1 = new Point(-4.28, 0.26);
    Point* p2 = new Point(-4.83, 0.73);
    TwoDimensionalVector* vec = new TwoDimensionalVector(p1, p2);
    Circle c(vec);
    Iterator* it;
    DFSIteratorFactory* dfs_factory = new DFSIteratorFactory();
    ASSERT_NO_THROW(it = c.createIterator(dfs_factory));
    delete p1;
    delete p2;
    delete vec;
    delete it;
    delete dfs_factory;
}

TEST(CircleTest, CreateBFSIteratorTest){
    Point* p1 = new Point(-4.28, 0.26);
    Point* p2 = new Point(-4.83, 0.73);
    TwoDimensionalVector* vec = new TwoDimensionalVector(p1, p2);
    Circle c(vec);
    Iterator* it;
    BFSIteratorFactory* bfs_factory = new BFSIteratorFactory();
    ASSERT_NO_THROW(it = c.createIterator(bfs_factory));
    delete p1;
    delete p2;
    delete vec;
    delete it;
    delete bfs_factory;
}

TEST(CircleTest, CreateListIteratorTest){
    Point* p1 = new Point(-4.28, 0.26);
    Point* p2 = new Point(-4.83, 0.73);
    TwoDimensionalVector* vec = new TwoDimensionalVector(p1, p2);
    Circle c(vec);
    Iterator* it;
    ListIteratorFactory* list_factory = new ListIteratorFactory();
    ASSERT_NO_THROW(it = c.createIterator(list_factory));
    delete p1;
    delete p2;
    delete vec;
    delete it;
    delete list_factory;
}

TEST(CircleTest, AddShapeTest){
    Point* p1 = new Point(-4.28, 0.26);
    Point* p2 = new Point(-4.83, 0.73);
    TwoDimensionalVector* vec = new TwoDimensionalVector(p1, p2);
    Circle c1(vec);
    Shape* c2 = new Circle(vec);
    ASSERT_ANY_THROW(c1.addShape(c2));
    delete p1;
    delete p2;
    delete vec;
    delete c2;
}

TEST(CircleTest, DeleteShapeTest){
    Point* p1 = new Point(-4.28, 0.26);
    Point* p2 = new Point(-4.83, 0.73);
    TwoDimensionalVector* vec = new TwoDimensionalVector(p1, p2);
    Circle c1(vec);
    Shape* c2 = new Circle(vec);
    ASSERT_ANY_THROW(c1.deleteShape(c2));
    delete p1;
    delete p2;
    delete vec;
    delete c2;;
}

TEST(CircleTest, GetPointsTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 2);
    TwoDimensionalVector* vec = new TwoDimensionalVector(p1, p2);
    Circle c(vec);
    std::set<const Point*> circle_points = c.getPoints();
    Point p3(2, 2);
    Point p4(-2, -2);
    Point point_arr[] = {p3, p4};
    for (auto p : circle_points)
        ASSERT_TRUE(std::find(point_arr, point_arr+2, (*p)) != point_arr+2);
    delete p1;
    delete p2;
    delete vec;
}

TEST(CircleTest, CollisionDetectorTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 2);
    Point* p3 = new Point(0, 5);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    Circle* c1 = new Circle(vec1);
    Circle* c2 = new Circle(vec2);
    CollisionDetector* visitor = new CollisionDetector(c2);
    ASSERT_NO_THROW(c1->accept(visitor));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete c1;
    delete c2;
    delete visitor;
}