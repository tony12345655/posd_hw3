#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/rectangle.h"
#include "../src/iterator/factory/dfs_iterator_factory.h"
#include "../src/iterator/factory/bfs_iterator_factory.h"
#include "../src/iterator/factory/list_iterator_factory.h"
#include "../src/visitor/collision_detector.h"

#include <set>
#include <algorithm>

TEST(RectangleTets, LegalTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    Point* p3 = new Point(1, 0);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    TwoDimensionalVector* vec3 = new TwoDimensionalVector(p2, p1);
    TwoDimensionalVector* vec4 = new TwoDimensionalVector(p3, p1);
    Rectangle* rec1;
    Rectangle* rec2;
    Rectangle* rec3;
    Rectangle* rec4;
    // 向量A的x與向量B的x相同
    ASSERT_NO_THROW(rec1 = new Rectangle(vec1, vec2));
    // 向量A的x與向量B的y相同
    ASSERT_NO_THROW(rec2 = new Rectangle(vec1, vec4));
    // 向量A的y與向量B的x相同
    ASSERT_NO_THROW(rec3 = new Rectangle(vec3, vec2));
    // 向量A的y與向量B的y相同
    ASSERT_NO_THROW(rec4 = new Rectangle(vec3, vec4));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete vec3;
    delete vec4;
    delete rec1;
    delete rec2;
    delete rec3;
    delete rec4;
}

TEST(RectangleTest, IllegalTest){
    Point* p1 = new Point(1, 1);
    Point* p2 = new Point(2, 2);
    Point* p3 = new Point(0, 0);
    Point* p4 = new Point(0, 1);
    Point* p5 = new Point(1, 0);
    Point* p6 = new Point(0, 2);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p3, p4);
    TwoDimensionalVector* vec3 = new TwoDimensionalVector(p3, p5);
    TwoDimensionalVector* vec4 = new TwoDimensionalVector(p4, p1);
    TwoDimensionalVector* vec5 = new TwoDimensionalVector(p4, p6);
    Rectangle* rec1;
    Rectangle* rec2;
    Rectangle* rec3;
    Rectangle* rec4;
    // 兩向量無任一點重疊
    ASSERT_ANY_THROW(rec1 = new Rectangle(vec1, vec2));
    // 兩向量為同一向量
    ASSERT_ANY_THROW(rec2 = new Rectangle(vec2, vec2));
    // 兩向量平行
    ASSERT_ANY_THROW(rec3 = new Rectangle(vec3, vec4));
    // 兩向量正交但無任一點重疊
    ASSERT_ANY_THROW(rec4 = new Rectangle(vec2, vec5));
    delete p1;
    delete p2;
    delete p3;
    delete p4;
    delete p5;
    delete p6;
    delete vec1;
    delete vec2;
    delete vec3;
    delete vec4;
    delete vec5;
}

TEST(RectangleTest, LengthTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    Point* p3 = new Point(1, 0);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    Rectangle rec(vec1, vec2);
    ASSERT_NEAR(1, rec.length(), 0.001);
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
}

TEST(RectangleTest, WidthTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    Point* p3 = new Point(1, 0);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    Rectangle rec(vec1, vec2);
    ASSERT_NEAR(1, rec.width(), 0.001);
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
}

TEST(RectangleTest, AreaTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    Point* p3 = new Point(1, 0);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    Rectangle rec(vec1, vec2);
    ASSERT_NEAR(1, rec.area(), 0.001);
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
}

TEST(RectangleTest, PerimeterTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    Point* p3 = new Point(1, 0);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    Rectangle rec(vec1, vec2);
    ASSERT_NEAR(4, rec.perimeter(), 0.001);
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
}

TEST(RectangleTest, InfoTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    Point* p3 = new Point(1, 0);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    Rectangle rec(vec1, vec2);
    ASSERT_EQ("Rectangle (Vector ((0.00, 0.00), (0.00, 1.00)), Vector ((0.00, 0.00), (1.00, 0.00)))", rec.info());
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
}

TEST(RectangleTest, createDFSIteratorTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    Point* p3 = new Point(1, 0);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    Rectangle rec(vec1, vec2);
    Iterator* it;
    DFSIteratorFactory* dfs_factory = new DFSIteratorFactory();
    ASSERT_NO_THROW(it = rec.createIterator(dfs_factory));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete it;
    delete dfs_factory;
}

TEST(RectangleTest, createBFSIteratorTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    Point* p3 = new Point(1, 0);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    Rectangle rec(vec1, vec2);
    Iterator* it;
    BFSIteratorFactory* bfs_factory = new BFSIteratorFactory();
    ASSERT_NO_THROW(it = rec.createIterator(bfs_factory));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete it;
    delete bfs_factory;
}

TEST(RectangleTest, createListIteratorTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    Point* p3 = new Point(1, 0);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    Rectangle rec(vec1, vec2);
    Iterator* it;
    ListIteratorFactory* list_factory = new ListIteratorFactory();
    ASSERT_NO_THROW(it = rec.createIterator(list_factory));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete it;
    delete list_factory;
}

TEST(RectangleTest, AddShape){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    Point* p3 = new Point(1, 0);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    Rectangle rec1(vec1, vec2);
    Shape* rec2 = new Rectangle(vec1, vec2);
    ASSERT_ANY_THROW(rec1.addShape(rec2));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete rec2;
}

TEST(RectangleTest, DeleteShape){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    Point* p3 = new Point(1, 0);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    Rectangle rec1(vec1, vec2);
    Shape* rec2 = new Rectangle(vec1, vec2);
    ASSERT_ANY_THROW(rec1.deleteShape(rec2));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete rec2;
}

TEST(RectangleTest, GetPointsTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 2);
    Point* p3 = new Point(2, 0);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    Rectangle rec(vec1, vec2);
    std::set<const Point*> rectangle_points = rec.getPoints();
    Point p4(2, 2);
    Point point_arr[] = {(*p1), (*p2), (*p3), p4};
    for (auto p : rectangle_points)
        ASSERT_TRUE(std::find(point_arr, point_arr+4, (*p)) != point_arr+4);
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
}

TEST(RectangleTest, CollisionDetectorTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 2);
    Point* p3 = new Point(2, 0);
    Point* p4 = new Point(5, 0);
    Point* p5 = new Point(0, 5);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    TwoDimensionalVector* vec3 = new TwoDimensionalVector(p1, p4);
    TwoDimensionalVector* vec4 = new TwoDimensionalVector(p1, p5);
    Rectangle* rec1 = new Rectangle(vec1, vec2);
    Rectangle* rec2 = new Rectangle(vec3, vec4);
    CollisionDetector* visitor = new CollisionDetector(rec2);
    ASSERT_NO_THROW(rec1->accept(visitor));
    delete p1;
    delete p2;
    delete p3;
    delete p4;
    delete p5;
    delete vec1;
    delete vec2;
    delete vec3;
    delete vec4;
    delete rec1;
    delete rec2;
    delete visitor;
}