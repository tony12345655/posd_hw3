#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/triangle.h"
#include "../src/iterator/factory/dfs_iterator_factory.h"
#include "../src/iterator/factory/bfs_iterator_factory.h"
#include "../src/iterator/factory/list_iterator_factory.h"
#include "../src/visitor/collision_detector.h"

#include <set>
#include <algorithm>

TEST(TriangleTst, LegalTest){
    Point* p1 = new Point(3, 0);
    Point* p2 = new Point(1, 0);
    Point* p3 = new Point(7, 1);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p1, p3);
    TwoDimensionalVector* vec3 = new TwoDimensionalVector(p3, p1);
    TwoDimensionalVector* vec4 = new TwoDimensionalVector(p2, p1);
    Triangle* tri1;
    Triangle* tri2;
    Triangle* tri3;
    Triangle* tri4;
    // 向量A的x與向量B的x相同
    ASSERT_NO_THROW(tri1 = new Triangle(vec1, vec2));
    // 向量A的x與向量B的y相同
    ASSERT_NO_THROW(tri2 = new Triangle(vec1, vec3));
    // 向量A的y與向量B的x相同
    ASSERT_NO_THROW(tri3 = new Triangle(vec4, vec2));
    // 向量A的y與向量B的y相同
    ASSERT_NO_THROW(tri4 = new Triangle(vec4, vec3));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete vec3;
    delete vec4;
    delete tri1;
    delete tri2;
    delete tri3;
    delete tri4;

}

TEST(TriangleTest, IllegalTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(1, 0);
    Point* p3 = new Point(3, 4);
    Point* p4 = new Point(3, 0);
    Point* p5 = new Point(2, 2);
    Point* p6 = new Point(0, 2);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p3, p4);
    TwoDimensionalVector* vec3 = new TwoDimensionalVector(p3, p2);
    TwoDimensionalVector* vec4 = new TwoDimensionalVector(p5, p6);
    Triangle* tri1;
    Triangle* tri2;
    Triangle* tri3;
    Triangle* tri4;
    // 兩向量無任一點重疊
    ASSERT_ANY_THROW(tri1 = new Triangle(vec1, vec2));
    // 兩向量為同一向量
    ASSERT_ANY_THROW(tri2 = new Triangle(vec3, vec3));
    // 兩向量平行
    ASSERT_ANY_THROW(tri3 = new Triangle(vec1, vec4));
    delete p1;
    delete p2;
    delete p3;
    delete p4;
    delete p5;
    delete p6;
    delete vec1;
    delete vec2;
    delete vec3;
    delete vec4;
}

TEST(TriangleTest, AreaTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(3, 0);
    Point* p3 = new Point(3, 4);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p3, p2);
    Triangle tri(vec1, vec2);
    ASSERT_NEAR(6, tri.area(), 0.001);
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
}

TEST(TriangleTest, PerimeterTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(3, 0);
    Point* p3 = new Point(3, 4);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p3, p2);
    Triangle tri(vec1, vec2);
    ASSERT_NEAR(12, tri.perimeter(), 0.001);
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
}

TEST(TriangleTest, InfoTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(3, 0);
    Point* p3 = new Point(3, 4);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p3, p2);
    Triangle tri(vec1, vec2);
    ASSERT_EQ("Triangle (Vector ((0.00, 0.00), (3.00, 0.00)), Vector ((3.00, 4.00), (3.00, 0.00)))", tri.info());
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
}

TEST(TriangleTest, CreateDFSIteratorTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(3, 0);
    Point* p3 = new Point(3, 4);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p3, p2);
    Triangle tri(vec1, vec2);
    Iterator* it;
    DFSIteratorFactory* dfs_factory = new DFSIteratorFactory();
    ASSERT_NO_THROW(it = tri.createIterator(dfs_factory));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete it;
    delete dfs_factory;
}

TEST(TriangleTest, CreateBFSIteratorTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(3, 0);
    Point* p3 = new Point(3, 4);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p3, p2);
    Triangle tri(vec1, vec2);
    Iterator* it;
    BFSIteratorFactory* bfs_factory = new BFSIteratorFactory();
    ASSERT_NO_THROW(it = tri.createIterator(bfs_factory));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete it;
    delete bfs_factory;
}

TEST(TriangleTest, CreateListIteratorTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(3, 0);
    Point* p3 = new Point(3, 4);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p3, p2);
    Triangle tri(vec1, vec2);
    Iterator* it;
    BFSIteratorFactory* list_factory = new BFSIteratorFactory();
    ASSERT_NO_THROW(it = tri.createIterator(list_factory));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete it;
    delete list_factory;
}

TEST(TriangleTest, AddShapeTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(3, 0);
    Point* p3 = new Point(3, 4);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p3, p2);
    Triangle tri1(vec1, vec2);
    Shape* tri2 = new Triangle(vec1, vec2);
    ASSERT_ANY_THROW(tri1.addShape(tri2));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete tri2;
}

TEST(TriangleTest, DeleteShapeTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(3, 0);
    Point* p3 = new Point(3, 4);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p3, p2);
    Triangle tri1(vec1, vec2);
    Shape* tri2 = new Triangle(vec1, vec2);
    ASSERT_ANY_THROW(tri1.deleteShape(tri2));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete tri2;
}

TEST(TriangleTest, GetPointsTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(3, 0);
    Point* p3 = new Point(3, 4);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p3, p2);
    Triangle tri(vec1, vec2);
    std::set<const Point*> triangle_points = tri.getPoints();
    Point point_arr[] = {(*p1), (*p2), (*p3)};
    for (auto p : triangle_points)
        ASSERT_TRUE(std::find(point_arr, point_arr+3, (*p)) != point_arr+3);
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
}

TEST(TriangleTest, CollisionDetectorTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(3, 0);
    Point* p3 = new Point(3, 4);
    TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
    TwoDimensionalVector* vec2 = new TwoDimensionalVector(p3, p2);
    Triangle* tri1 = new Triangle(vec1, vec2);
    Triangle* tri2 = new Triangle(vec1, vec2);
    CollisionDetector* visitor = new CollisionDetector(tri2);
    ASSERT_NO_THROW(tri1->accept(visitor));
    delete p1;
    delete p2;
    delete p3;
    delete vec1;
    delete vec2;
    delete tri1;
    delete tri2;
    delete visitor;
}