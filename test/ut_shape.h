#include <vector>
#include "../src/point.h"
#include "../src/two_dimensional_vector.h"
#include "../src/shape.h"
#include "../src/triangle.h"
#include "../src/rectangle.h"
#include "../src/circle.h"
#include "../src/compound_shape.h"
#include "../src/iterator/factory/list_iterator_factory.h"
#include "../src/iterator/list_compound_iterator.h"
#include "../src/visitor/collision_detector.h"

class ShapeTest : public ::testing::Test {
    protected:
        Point* p1 = new Point(0, 0);
        Point* p2 = new Point(3, 0);
        Point* p3 = new Point(3, 4);
        Point* p4 = new Point(0, 1);
        Point* p5 = new Point(2, 0);
        TwoDimensionalVector* vec1 = new TwoDimensionalVector(p1, p2);
        TwoDimensionalVector* vec2 = new TwoDimensionalVector(p3, p2);
        TwoDimensionalVector* vec3 = new TwoDimensionalVector(p1, p4);
        TwoDimensionalVector* vec4 = new TwoDimensionalVector(p1, p5);
        Triangle* tri;
        Rectangle* rec;
        Circle* cir;

        void SetUp() override {
            tri = new Triangle(vec1, vec2);
            rec = new Rectangle(vec3, vec4);
            cir = new Circle(vec3);
        }

        void TearDown() override {
            delete p1;
            delete p2;
            delete p3;
            delete p4;
            delete p5;
            delete vec1;
            delete vec2;
            delete vec3;
            delete vec4;
            delete tri;
            delete rec;
            delete cir;
        }
};

TEST_F(ShapeTest, AreaPolymorphismTest){
    CompoundShape* compound_shape = new CompoundShape();
    std::vector<Shape*> shape_vector = {tri, rec, cir, compound_shape};
    ASSERT_NEAR(tri->area(), shape_vector[0]->area(), 0.001);
    ASSERT_NEAR(rec->area(), shape_vector[1]->area(), 0.001);
    ASSERT_NEAR(cir->area(), shape_vector[2]->area(), 0.001);
    ASSERT_NEAR(compound_shape->area(), shape_vector[3]->area(), 0.001);
    delete compound_shape;
}

TEST_F(ShapeTest, PerimeterPolymorphismTest){
    CompoundShape* compound_shape = new CompoundShape();
    std::vector<Shape*> shape_vector = {tri, rec, cir, compound_shape};
    ASSERT_NEAR(tri->perimeter(), shape_vector[0]->perimeter(), 0.001);
    ASSERT_NEAR(rec->perimeter(), shape_vector[1]->perimeter(), 0.001);
    ASSERT_NEAR(cir->perimeter(), shape_vector[2]->perimeter(), 0.001);
    ASSERT_NEAR(compound_shape->perimeter(), shape_vector[3]->perimeter(), 0.001);
    delete compound_shape;
}

TEST_F(ShapeTest, InfoPolymorphismTest){
    CompoundShape* compound_shape = new CompoundShape();
    std::vector<Shape*> shape_vector = {tri, rec, cir, compound_shape};
    ASSERT_EQ(tri->info(), shape_vector[0]->info());
    ASSERT_EQ(rec->info(), shape_vector[1]->info());
    ASSERT_EQ(cir->info(), shape_vector[2]->info());
    ASSERT_EQ(compound_shape->info(), shape_vector[3]->info());
    delete compound_shape;
}

TEST_F(ShapeTest, AddShapePolymorphismTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    TwoDimensionalVector* vec = new TwoDimensionalVector(p1, p2);
    Shape* new_shape = new Circle(vec);
    CompoundShape* compound_shape = new CompoundShape();
    std::vector<Shape*> shape_vector = {tri, rec, cir, compound_shape};
    ASSERT_ANY_THROW(tri->addShape(new_shape));
    ASSERT_ANY_THROW(shape_vector[0]->addShape(new_shape));
    ASSERT_ANY_THROW(rec->addShape(new_shape));
    ASSERT_ANY_THROW(shape_vector[1]->addShape(new_shape));
    ASSERT_ANY_THROW(cir->addShape(new_shape));
    ASSERT_ANY_THROW(shape_vector[2]->addShape(new_shape));
    compound_shape->addShape(new_shape);
    shape_vector[3]->addShape(new_shape);
    ASSERT_NEAR(compound_shape->area(), shape_vector[3]->area(), 0.001);
    delete p1;
    delete p2;
    delete vec;
    delete new_shape;
    delete compound_shape;
}

TEST_F(ShapeTest, DeleteShapePolymorphismTest){
    Point* p1 = new Point(0, 0);
    Point* p2 = new Point(0, 1);
    TwoDimensionalVector* vec = new TwoDimensionalVector(p1, p2);
    Shape* new_shape = new Circle(vec);
    CompoundShape* compound_shape = new CompoundShape();
    std::vector<Shape*> shape_vector = {tri, rec, cir, compound_shape};
    ASSERT_ANY_THROW(tri->deleteShape(new_shape));
    ASSERT_ANY_THROW(shape_vector[0]->deleteShape(new_shape));
    ASSERT_ANY_THROW(rec->deleteShape(new_shape));
    ASSERT_ANY_THROW(shape_vector[1]->deleteShape(new_shape));
    ASSERT_ANY_THROW(cir->deleteShape(new_shape));
    ASSERT_ANY_THROW(shape_vector[2]->deleteShape(new_shape));
    compound_shape->addShape(new_shape);
    shape_vector[3]->addShape(new_shape);
    compound_shape->deleteShape(new_shape);
    shape_vector[3]->deleteShape(new_shape);
    ASSERT_NEAR(compound_shape->area(), shape_vector[3]->area(), 0.001);
    delete p1;
    delete p2;
    delete vec;
    delete new_shape;
    delete compound_shape;
}

TEST_F(ShapeTest, CreateIteratorPolymorphismTest){
    CompoundShape* compound_shape = new CompoundShape();
    compound_shape->addShape(cir);
    std::vector<Shape*> shape_vector = {tri, rec, cir, compound_shape};
    ListIteratorFactory* tri_list_factory = new ListIteratorFactory();
    Iterator* tri_it = tri->createIterator(tri_list_factory);
    ListIteratorFactory* vec_tri_list_factory = new ListIteratorFactory();
    Iterator* vec_tri_it = shape_vector[0]->createIterator(vec_tri_list_factory);
    ListIteratorFactory* rec_list_factory = new ListIteratorFactory();
    Iterator* rec_it = rec->createIterator(rec_list_factory);
    ListIteratorFactory* vec_rec_list_factory = new ListIteratorFactory();
    Iterator* vec_rec_it = shape_vector[1]->createIterator(vec_rec_list_factory);
    ListIteratorFactory* cir_list_factory = new ListIteratorFactory();
    Iterator* cir_it = rec->createIterator(cir_list_factory);
    ListIteratorFactory* vec_cir_list_factory = new ListIteratorFactory();
    Iterator* vec_cir_it = shape_vector[2]->createIterator(vec_cir_list_factory);
    ListIteratorFactory* compound_shape_list_factory = new ListIteratorFactory();
    Iterator* compound_shape_it = rec->createIterator(compound_shape_list_factory);
    ListIteratorFactory* vec_compound_shape_list_factory = new ListIteratorFactory();
    Iterator* vec_compound_shape_it = shape_vector[2]->createIterator(vec_compound_shape_list_factory);
    ASSERT_EQ(tri_it->isDone(), vec_tri_it->isDone());
    ASSERT_EQ(rec_it->isDone(), vec_rec_it->isDone());
    ASSERT_EQ(cir_it->isDone(), vec_cir_it->isDone());
    ASSERT_EQ(compound_shape_it->isDone(), vec_compound_shape_it->isDone());
    delete compound_shape;
    delete tri_list_factory;
    delete tri_it;
    delete vec_tri_list_factory;
    delete vec_tri_it;
    delete rec_list_factory;
    delete rec_it;
    delete vec_rec_list_factory;
    delete vec_rec_it;
    delete cir_list_factory;
    delete cir_it;
    delete vec_cir_list_factory;
    delete vec_cir_it;
    delete compound_shape_list_factory;
    delete compound_shape_it;
    delete vec_compound_shape_list_factory;
    delete vec_compound_shape_it;
}

TEST_F(ShapeTest, GetPointsPolymorphismTest){
    CompoundShape* compound_shape = new CompoundShape();
    compound_shape->addShape(cir);
    std::vector<Shape*> shape_vector = {tri, rec, cir, compound_shape};
    ASSERT_EQ(tri->getPoints(), shape_vector[0]->getPoints());
    ASSERT_EQ(rec->getPoints(), shape_vector[1]->getPoints());
    ASSERT_EQ(cir->getPoints(), shape_vector[2]->getPoints());
    ASSERT_EQ(compound_shape->getPoints(), shape_vector[3]->getPoints());
    delete compound_shape;
}

TEST_F(ShapeTest, AcceptPolymorphismTest){
    CompoundShape* compound_shape = new CompoundShape();
    compound_shape->addShape(cir);
    std::vector<Shape*> shape_vector = {tri, rec, cir, compound_shape};
    CollisionDetector* visitor = new CollisionDetector(cir);
    CollisionDetector* vec_visitor = new CollisionDetector(cir);
    tri->accept(visitor);
    shape_vector[0]->accept(vec_visitor);
    ASSERT_EQ(visitor->collidedShapes().size(), vec_visitor->collidedShapes().size());
    rec->accept(visitor);
    shape_vector[1]->accept(vec_visitor);
    ASSERT_EQ(visitor->collidedShapes().size(), vec_visitor->collidedShapes().size());
    cir->accept(visitor);
    shape_vector[2]->accept(vec_visitor);
    ASSERT_EQ(visitor->collidedShapes().size(), vec_visitor->collidedShapes().size());
    delete compound_shape;
    delete visitor;
    delete vec_visitor;
}