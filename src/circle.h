#pragma once

#include "point.h"
#include "two_dimensional_vector.h"
#include "shape.h"
#include "iterator/factory/iterator_factory.h"
#include "visitor/shape_visitor.h"

#include <string>
#include <cmath>
#include <sstream>
#include <set>

class Circle : public Shape
{
private:
    TwoDimensionalVector *_radiusVec;
    const Point* _max;
    const Point* _min;

public:
    Circle(TwoDimensionalVector *radiusVec):_radiusVec(radiusVec) {
        this->_max = new Point(this->_radiusVec->a()->x() + this->radius(), this->_radiusVec->a()->y() + this->radius());
        this->_min = new Point(this->_radiusVec->a()->x() - this->radius(), this->_radiusVec->a()->y() - this->radius());
    }

    ~Circle() {
        delete this->_max;
        delete this->_min;
    }

    double radius() const { return _radiusVec->length(); }

    double area() const override {
        return pow(this->radius(), 2) * M_PI;
    }

    double perimeter() const override {
        return 2 * this->radius() * M_PI;
    }

    std::string info() const override {
        std::stringstream ss;
        ss << std::fixed << std::setprecision(2) << "Circle (" << _radiusVec->info() << ")";
        std::string out = ss.str();
        
        return out;    
    }
    
    Iterator *createIterator(IteratorFactory *factory) override {
        return factory->createIterator();
    }

    void addShape(Shape* shape) override {
        return throw "Circle can't addShape.";
    }

    void deleteShape(Shape* shape) override {
        return throw "Circle can't deleteShape.";
    }

    std::set<const Point*> getPoints() override {
        std::set<const Point*> vertex{this->_max, this->_min};
        return vertex;
    }

    void accept(ShapeVisitor* visitor) override {
        return visitor->visitCircle(this);
    };
};