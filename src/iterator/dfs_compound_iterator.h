#pragma once

#include "iterator.h"
#include "../shape.h"
#include "./factory/iterator_factory.h"
#include "./factory/dfs_iterator_factory.h"

#include <list>
#include <stack>

class CompoundShape;

template<class ForwardIterator>
class DFSCompoundIterator : public Iterator
{
private:
    std::list<Shape* > _shape_list;
    std::stack<Shape *> _shape_stack;
public:
    DFSCompoundIterator(ForwardIterator begin, ForwardIterator end) : _shape_list(begin, end) {
        this->first();
    }

    ~DFSCompoundIterator() {}

    void first() override {
        while (!this->_shape_stack.empty()) this->_shape_stack.pop();
        for (auto shape_it = this->_shape_list.rbegin(); shape_it != this->_shape_list.rend(); ++shape_it)
            this->_shape_stack.push(*shape_it);
    }

    Shape* currentItem() const override {
        if (!this->isDone())
            return this->_shape_stack.top();
        else
            throw "NULL";
    }

    void next() override {
        if (!this->isDone()){
            DFSIteratorFactory* dfs_factory = new DFSIteratorFactory();
            Iterator* shape_it = this->_shape_stack.top()->createIterator(dfs_factory);
            this->_shape_stack.pop();
            if (!shape_it->isDone()){
                DFSCompoundIterator* dfs_it = (DFSCompoundIterator*)shape_it;
                for (auto it = dfs_it->_shape_list.rbegin(); it != dfs_it->_shape_list.rend(); ++it)
                    this->_shape_stack.push(*it);
            }
            delete dfs_factory;
            delete shape_it;
        }
        else
            throw "Can't next.";
    }

    bool isDone() const override {
        return this->_shape_stack.empty();
    }
};