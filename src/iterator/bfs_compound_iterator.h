#pragma once

#include "iterator.h"
#include "../shape.h"
#include "./factory/iterator_factory.h"
#include "./factory/bfs_iterator_factory.h"

#include <list>
#include <queue>

class CompoundShape;

template<class ForwardIterator>
class BFSCompoundIterator : public Iterator
{
private:
    std::list<Shape* > _shape_list;
    std::queue<Shape *> _shape_queue;

public:
    BFSCompoundIterator(ForwardIterator begin, ForwardIterator end) : _shape_list(begin, end) {
        this->first();
    }

    ~BFSCompoundIterator() {}

    void first() override {
        while (!this->_shape_queue.empty()) this->_shape_queue.pop();
        for (auto shape_it = this->_shape_list.begin(); shape_it != this->_shape_list.end(); ++shape_it)
            this->_shape_queue.push(*shape_it);
    }

    Shape* currentItem() const override {
        if (!this->isDone())
            return this->_shape_queue.front();
        else
            throw "NULL";
    }

    void next() override {
        if (!this->isDone()){
            BFSIteratorFactory* bfs_factory = new BFSIteratorFactory();
            Iterator* shape_it = this->_shape_queue.front()->createIterator(bfs_factory);
            this->_shape_queue.pop();
            if (!shape_it->isDone()){
                BFSCompoundIterator* bfs_it = (BFSCompoundIterator*)shape_it;
                for (auto it = bfs_it->_shape_list.begin(); it != bfs_it->_shape_list.end(); ++it)
                    this->_shape_queue.push(*it);
            }
            delete bfs_factory;
            delete shape_it;
        }
        else
            throw "Can't next.";
    }

    bool isDone() const override {
        return this->_shape_queue.empty();
    }
};
