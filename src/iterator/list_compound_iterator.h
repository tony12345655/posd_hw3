#pragma once

#include "./iterator.h"
#include "../shape.h"

#include <list>

template <class ForwardIterator>
class ListCompoundIterator : public Iterator
{
private:
    ForwardIterator _current, _begin, _end;
public:
    ListCompoundIterator(ForwardIterator begin, ForwardIterator end) : _begin(begin), _end(end) {
        this->first();
    }

    void first() override {
        this->_current =  this->_begin;
    }

    Shape *currentItem() const override {
        if (!this->isDone())
            return *(this->_current);
        else
            throw "NULL";
    }

    void next() override {
        if (!this->isDone())
            ++this->_current;
        else
            throw "Can't next.";
    }

    bool isDone() const override {
        return this->_current == this->_end;
    }
};
