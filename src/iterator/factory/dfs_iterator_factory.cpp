#include "dfs_iterator_factory.h"
#include "../../shape.h"
#include "../null_iterator.h"
#include "../dfs_compound_iterator.h"

#include <list>

Iterator* DFSIteratorFactory::createIterator(){
    return new NullIterator();
}

Iterator* DFSIteratorFactory::createIterator(std::list<Shape *>::const_iterator begin, std::list<Shape *>::const_iterator end){
    return new DFSCompoundIterator(begin, end);
}