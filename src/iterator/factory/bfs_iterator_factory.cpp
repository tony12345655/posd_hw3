#include "bfs_iterator_factory.h"
#include "../../shape.h"
#include "../null_iterator.h"
#include "../bfs_compound_iterator.h"

#include <list>

Iterator* BFSIteratorFactory::createIterator(){
    return new NullIterator();
}

Iterator* BFSIteratorFactory::createIterator(std::list<Shape *>::const_iterator begin, std::list<Shape *>::const_iterator end){
    return new BFSCompoundIterator(begin, end);
}