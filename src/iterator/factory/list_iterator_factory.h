#pragma once

#include "./iterator_factory.h"
#include "../null_iterator.h"
#include "../list_compound_iterator.h"
#include "../../shape.h"

class ListIteratorFactory : public IteratorFactory {
    public:
        ListIteratorFactory() {}

        Iterator* createIterator() override {
            return new NullIterator();
        }

        Iterator* createIterator(std::list<Shape *>::const_iterator begin, std::list<Shape *>::const_iterator end) override {
            return new ListCompoundIterator(begin, end);
        }
};