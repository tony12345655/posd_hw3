#pragma once

#include "./iterator_factory.h"
#include "../iterator"
#include "../../shape.h"

class DFSIteratorFactory : public IteratorFactory {
    public: 
        DFSIteratorFactory() {}
        Iterator* createIterator() override;
        Iterator* createIterator(std::list<Shape *>::const_iterator begin, std::list<Shape *>::const_iterator end) override;
};