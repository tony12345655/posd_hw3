#pragma once

#include "point.h"
#include "shape.h"
#include "iterator/factory/iterator_factory.h"
#include "iterator/factory/dfs_iterator_factory.h"
#include "visitor/shape_visitor.h"

#include <list>
#include <sstream>
#include <set>

class CompoundShape : public Shape
{
private:
    std::list<Shape *> _shapes;

public:
    CompoundShape(Shape **shapes, int size): _shapes(shapes, shapes+size) {}

    CompoundShape() {}

    double area() const override {
        double num_area = 0;
        for (auto shape : this->_shapes)
            num_area += shape->area();
        return num_area;        
    }

    double perimeter() const override {
        double num_perimeter = 0;
        for (auto shape : this->_shapes)
            num_perimeter += shape->perimeter();
        return num_perimeter;  
    }

    std::string info() const override {
        std::string out = "CompoundShape (";
        for (auto shape : this->_shapes)
            out += (shape->info() + ", ");
        if (this->_shapes.size() != 0)
            out.erase(out.size()-2, 2);
        out += ")";
        return out;
    }

    Iterator *createIterator(IteratorFactory *factory) override {
        std::list<Shape *>::const_iterator begin = this->_shapes.cbegin();
        std::list<Shape *>::const_iterator end = this->_shapes.cend();
        return factory->createIterator(begin, end);
    }

    void addShape(Shape* shape) override{
        this->_shapes.push_back(shape);
    }

    void deleteShape(Shape* shape) override{
        Shape* deleted_point = nullptr;
        for(auto list_it=this->_shapes.begin(); list_it != this->_shapes.end() && deleted_point == nullptr; ++list_it){
            DFSIteratorFactory* dfs_factory = new DFSIteratorFactory();
            Iterator* shape_it = (*list_it)->createIterator(dfs_factory);
            if ((*list_it) == shape){
                deleted_point = shape;
            }
            if (!shape_it->isDone())
                (*list_it)->deleteShape(shape);
            delete dfs_factory;
            delete shape_it;
        }
        if (deleted_point != nullptr)
            this->_shapes.remove(deleted_point);
    }

    std::set<const Point*> getPoints() {
        std::set<const Point*> vertex;
        for(auto list_it=this->_shapes.begin(); list_it != this->_shapes.end(); ++list_it){
            std::set<const Point*> shape_point = (*list_it)->getPoints();
            for (auto shape_set_it = shape_point.begin(); shape_set_it != shape_point.end(); ++shape_set_it)
                vertex.insert(*shape_set_it);
        }
        return vertex;
    };

    void accept(ShapeVisitor* visitor) override {
        return visitor->visitCompoundShape(this);
    };
};
